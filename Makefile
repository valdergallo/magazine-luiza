ExecDocker = docker-compose exec app

help:
	@echo "up		Docker compose up"
	@echo "down		Docker compose down"
	@echo "test		Run pytest"
	@echo "ptw		Run pytest-whatcher"
	@echo "run		Run Matrix to Image"
	@echo "bash		Run bash in Image"

up:
	docker-compose up -d

down:
	docker-compose down

.PHONY: test
test:
	$(ExecDocker) pytest -v

ptw:
	$(ExecDocker) ptw -- -v

.PHONY: run
run:
	$(ExecDocker) python manage.py migrate
	$(ExecDocker) python manage.py runserver 0.0.0.0:8000

.PHONY: bash
bash:
	$(ExecDocker) bash
