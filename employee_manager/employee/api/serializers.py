# -*- coding: utf-8 -*-
from rest_framework import serializers
from employee.models import Department, Employee


class EmployeeSerializer(serializers.ModelSerializer):
    """
    Serialize Employee instance and save

    attr:
        name: is the first_name and last_name
        email: employee unique key
        department: Department name
    """
    name = serializers.CharField(max_length=250, required=True)
    email = serializers.EmailField()
    department = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='name'
    )

    class Meta:
        model = Employee
        lookup_field = "email"
        fields = ('name', 'email', 'department')

    def get_or_create_department(self, department_name):
        """
        Create one new department or return one
        """
        if not department_name:
            return
        department, _ = Department.objects.get_or_create(name=department_name)
        return department

    def parse_content(self, validated_data):
        """
        Split name to first_name and last_name
        Convert Department Name to Department instance
        """
        first_name, last_name = validated_data['name'].split(' ', 1)
        validated_data['first_name'] = first_name
        validated_data['last_name'] = last_name
        validated_data['username'] = first_name + last_name
        del validated_data['name']
        validated_data['department'] = self.get_or_create_department(
            self._kwargs['data']['department']
        )
        return validated_data

    def create(self, validated_data):
        validated_data = self.parse_content(validated_data)
        return Employee(**validated_data)

    def update(self, instance, validated_data):
        """
        Update one register using email as key
        """
        validated_data = self.parse_content(validated_data)

        instance.first_name = validated_data['first_name']
        instance.last_name = validated_data['last_name']
        instance.department = validated_data['department']
        instance.save()
        return instance
