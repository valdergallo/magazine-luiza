# -*- coding: utf-8 -*-
from rest_framework import viewsets
from employee.api.serializers import EmployeeSerializer
from employee.models import Employee
from django.shortcuts import get_object_or_404

class EmployeeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows employees to be viewed or edited.

    retrieve:
    Return the given employee.

    list:
    Return a list of all the existing employees.

    create:
    Create a new employee instance.

    update:
    Update a employee instance using email as key.

    delete:
    Delete a employee instance using email as key.
    """
    queryset = Employee.objects.select_related(
        'department').order_by('-date_joined')

    serializer_class = EmployeeSerializer
    lookup_field = "email"
    lookup_value_regex = r"[^@]+@[^@]+\.[^@]+"
