# -*- coding: utf-8 -*-
import pytest
from chance import chance
from employee.models import Employee
from employee.models import Department
from rest_framework.test import APIClient

pytest.mark.django_db


@pytest.fixture(autouse=True)
def department(db):
    return Department.objects.create(
        name=chance.name()
    )


@pytest.fixture(autouse=True)
def employee(department):
    return Employee.objects.create_user(
        username=chance.name(),
        email=chance.email(),
        password=chance.hex_hash(),
        department=department,
    )


@pytest.fixture(scope="module")
def client():
    return APIClient()


@pytest.fixture
def auth_client(employee):
    client = APIClient()
    client.force_authenticate(user=employee)
    return client
