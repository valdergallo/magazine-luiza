# -*- coding: utf-8 -*-
import pytest


def test_get_one_user_without_login(employee, client):
    response = client.get('/api/employee/')
    assert response.status_code == 403


def test_get_one_user(employee, auth_client):
    # XXX: error in python3.6
    # with django_assert_num_queries(1):
    response = auth_client.get('/api/employee/')
    assert response.status_code == 200
    assert len(response.data) == 1


def test_get_one_user_by_look_up(employee, auth_client):
    response = auth_client.get('/api/employee/%s/' % employee.email)
    assert response.status_code == 200
    assert response.data['name'] == employee.get_full_name()
    assert response.data['email'] == employee.email
    assert response.data['department'] == employee.department.name


def test_create_one_user(employee, auth_client):
    response = auth_client.post('/api/employee/', {
        "name": "Darth Vader",
        "email": "darth.vader@luizalabs.com",
        "department": "Architecture",
    })

    assert response.status_code == 201
    assert response.data['name'] == "Darth Vader"
    assert response.data['email'] == "darth.vader@luizalabs.com"
    assert response.data['department'] == "Architecture"


def test_update_by_email_one_user(employee, auth_client):
    response = auth_client.post('/api/employee/', {
        "name": "Darth Vader",
        "email": employee.email,
        "department": "Architecture",
    })

    assert response.status_code == 201
    assert response.data['name'] == "Darth Vader"
    assert response.data['email'] == employee.email
    assert response.data['department'] == "Architecture"


def test_delete_by_email(employee, auth_client):
    response = auth_client.delete('/api/employee/%s/' % employee.email)
    assert response.status_code == 204
