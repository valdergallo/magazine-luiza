# -*- coding: utf-8 -*-
from django.contrib import admin

from employee.models import Employee
from employee.models import Department


class EmployeeAdmin(admin.ModelAdmin):
    raw_id_fields = ("department",)

    list_display = (
        'username', 'name', 'email', 'is_staff', 'department'
    )
    search_fields = (
        'department__name', 'username', 'first_name', 'last_name', 'email'
    )
    list_filter = (
        'department',
    )


admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Department)
