# -*- coding: utf-8 -*-
from django.db import models

from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

class Employee(AbstractUser):
    email = models.EmailField(_('email address'), blank=True, unique=True)
    department = models.ForeignKey('Department', on_delete=models.DO_NOTHING,
                                   null=True, blank=True)

    class Meta:
        verbose_name = 'employee'
        verbose_name_plural = 'employees'

    def name(self):
        "Function to serialize name"
        return self.get_full_name()


class Department(models.Model):
    name = models.CharField(max_length=150, null=False, db_index=True,
                            unique=True)

    def __str__(self):
        return self.name
