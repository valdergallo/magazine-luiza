# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy
SITE_NAME = 'Luizalabs Employee Manager'


def config_admin_site(admin_site):

    # Text to put at the end of each page's <title>.
    admin_site.site_title = ugettext_lazy(SITE_NAME)

    # Text to put in each page's <h1> (and above login form).
    admin_site.site_header = ugettext_lazy(SITE_NAME)

    # Text to put at the top of the admin index page.
    admin_site.index_title = ugettext_lazy(SITE_NAME)

    admin_site.site_url = 'api/docs/'
