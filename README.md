# magazine-luiza

## To install

- using virtualenv

```bash
    makevirtualenv-3.6 .env
    source .env/bin/activate
    pip install -r requirements.txt
```

- using docker-composer to dev
```bash
    docker-compose -f docker/docker-compose.yml up
```

- using Makefile
```bash
    make up
```

- run server
```
    make run
```

- help maker
```
    make
```

- To test with bash command
```bash
curl -u admin:admin -H "Content-Type: application/javascript" http://localhost:8000/api/employee/
```

- to run tests
```bash
    make test
```

### Default Links

- Manager
    Default user is *admin* with pass *admin*
```
    http://localhost:8000/
```
![](media/magazine_admin.png?raw=true)


- Documents
```
    http://localhost:8000/api/docs/
```
![](media/magazine_docs.png?raw=true)

- API
```
    http://localhost:8000/api/employee/
```
![](media/magazine_drf_list.png?raw=true)


* I made one Makefile to run the defaults docker-compose commands. I'm using Python3.6 in this project.
